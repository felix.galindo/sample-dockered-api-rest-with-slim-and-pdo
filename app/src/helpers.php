<?php

function isset2(&$obj, $default = null) {
  return isset($obj) ? $obj : $default;
}

function between($value, $min, $max){
  return (($value >= $min) && ($value <= $max));
}