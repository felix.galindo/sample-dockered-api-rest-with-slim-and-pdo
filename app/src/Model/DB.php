<?php
namespace App\Model;

class DB {
  protected static ?\PDO $conn = null;

  public static function getConnection() : \PDO {
    if (is_null(static::$conn)) {
      static::$conn = new \PDO(PDO_CONNECTION_STRING, PDO_USER, PDO_PASSWORD, [ 
        \PDO::MYSQL_ATTR_FOUND_ROWS => true,
      ]);
    }
    return static::$conn;
  }
}
