<?php
namespace App\Model;

use App\Model\DB;
use App\Model\Color;

class ColorDAO {

  public static function getColors() : array {
    $conn = DB::getConnection();
    $sql = "SELECT * FROM colors";
    $result = $conn->query($sql);
    $colorsAssoc = $result->fetchAll(\PDO::FETCH_ASSOC);
    if (!$colorsAssoc) return [];
    $colors = [];
    foreach($colorsAssoc as $colorAssoc) {
      $colors[] = Color::fromAssoc($colorAssoc);
    }
    return $colors;
  }

  public static function getColorById(int $id) : ?Color {
    $conn = DB::getConnection();
    $sql = "SELECT * FROM colors WHERE id=:id";
    $statement = $conn->prepare($sql);
    $result = $statement->execute([
      ':id' => $id,
    ]);
    $colorAssoc = $statement->fetch(\PDO::FETCH_ASSOC);
    if (!$colorAssoc) return null;
    $color = Color::fromAssoc($colorAssoc);
    return $color;
  }

  public static function insertColor(Color $color) : ?Color {
    $conn = DB::getConnection();
    // Check the color has no ID set (must be null)
    if (!is_null($color->getId())) return null;
    $sql = "INSERT INTO colors VALUES (NULL, :name, :red, :green, :blue)";
    $statement = $conn->prepare($sql);
    $result = $statement->execute([
      ':name' => $color->getName(),
      ':red' => $color->getRed(),
      ':green' => $color->getGreen(),
      ':blue' => $color->getBlue(),
    ]);
    // check affected rows using rowCount to detect if the id didn't exist
    $newId = $conn->lastInsertId();
    if (!$newId) return null;
    $color->setId($newId);
    return $color;
  }

  public static function updateColor(Color $color) : ?Color {
    $conn = DB::getConnection();
    $sql = "UPDATE colors SET name=:name, red=:red, green=:green, blue=:blue WHERE id=:id";
    $statement = $conn->prepare($sql);
    $result = $statement->execute([
      ':id' => $color->getId(),
      ':name' => $color->getName(),
      ':red' => $color->getRed(),
      ':green' => $color->getGreen(),
      ':blue' => $color->getBlue(),
    ]);
    // check affected rows using rowCount to detect if the id didn't exist
    $count = $statement->rowCount();
    if ($count > 0) return $color;
    return null;
  }

  public static function deleteColorById(int $id) : bool {
    $conn = DB::getConnection();
    $sql = "DELETE FROM colors WHERE id=:id";
    $statement = $conn->prepare($sql);
    $result = $statement->execute([
      ':id' => $id,
    ]);
    // check affected rows using rowCount to detect if the id didn't exist
    $count = $statement->rowCount();
    return ($count > 0);
  }

  public static function deleteColor(Color $color) : bool {
    return $this->deleteColorById($color->getId());
  }

}
