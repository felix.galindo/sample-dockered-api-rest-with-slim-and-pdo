<?php
namespace App\Model;

// Sample model class for colors
class Color implements \JsonSerializable {
  private ?int $id;
  private string $name;
  private int $red;
  private int $green;
  private int $blue;

  // Constructor method
  public function __construct(?int $id, string $name, int $red, int $green, int $blue) {
    $this->id = $id;
    $this->name = $name;
    $this->red = $red;
    $this->green = $green;
    $this->blue = $blue;
  }

  // Trivial getters and setters
  public function getId() : ?int { return $this->id; }
  public function setId(?int $id) { $this->id = $id; }
  public function getName() : string { return $this->name; }
  public function setName(string $name) { $this->name = $name; }
  public function getRed() : int { return $this->red; }
  public function setRed(int $red) { $this->red = $red; }
  public function getGreen() : int { return $this->green; }
  public function setGreen(int $green) { $this->green = $green; }
  public function getBlue() : int { return $this->blue; }
  public function setBlue(int $blue) { $this->blue = $blue; }

  // Non-trivial getter
  public function getRGB() {
    return implode('', [
      '#', 
      sprintf("%02X", $this->red),
      sprintf("%02X", $this->green),
      sprintf("%02X", $this->blue)
    ]);
  }

  // Returns a new object based on the received associative array
  public static function fromAssoc(array $data) {
    return new Color(
      $data['id'], 
      $data['name'],
      $data['red'], 
      $data['green'],
      $data['blue']
    );
  }

  // Returns an associative array useful for JSON encoding
  public function jsonSerialize() : array {
    return [
      "id" => $this->id,
      "name" => $this->name,
      "red" => $this->red,
      "green" => $this->green,
      "blue" => $this->blue,
      "rgb" => $this->getRGB(),
    ];
  }

  // Trivial sample toString method
  public function __toString() : string {
    return $this->name . " = " . $this->getRGB();
  }

}