<?php
// Useful for debugging
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Paths
define('APP_FOLDER',  dirname(__DIR__));
define('HTML_FOLDER', APP_FOLDER . "/html");
define('LOG_FOLDER', APP_FOLDER . "/logs");
define('SRC_FOLDER', APP_FOLDER . "/src");

// Class autoloader
require_once APP_FOLDER . '/vendor/autoload.php';

// Useful helpers
require_once SRC_FOLDER . '/helpers.php';

// DB connection info
define('PDO_CONNECTION_STRING', "mysql:host=api_db;dbname=db");
define('PDO_USER', "dbuser");
define('PDO_PASSWORD', "dbpassword");

