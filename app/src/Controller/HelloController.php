<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class HelloController {

  public static function initRoutes($slim) {
    $slim->get('/hello/{name}', static::class . ':hello');
  }

  public function hello(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $name = $args['name'];
    $response->getBody()->write("Hello $name!");
    return $response;
  }

}