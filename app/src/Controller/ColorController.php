<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Model\ColorDAO;
use App\Model\Color;

class ColorController {

  public static function initRoutes($slim) {
    $slim->get('/color', static::class . ':getColors');
    $slim->get('/color/{id:[0-9]+}', static::class . ':getColorById');
    $slim->post('/color', static::class . ':postColor');
    $slim->patch('/color/{id:[0-9]+}', static::class . ':patchColorById');
    $slim->delete('/color/{id:[0-9]+}', static::class . ':deleteColorById');
  }

  public function getColors(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $colors = ColorDAO::getColors();
    $response = $response->withJson($colors);
    return $response;
  }

  public function getColorById(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $id = $args['id'];
    if (!is_numeric($id)) {
      // id must be a numeric value
      return $response->withStatus(400);
    }
    $color = ColorDAO::getColorById($id);
    if (is_null($color)) {
      // Color was not found in DB
      return $response->withStatus(404);
    }
    return $response->withJson($color);
  }

  public function postColor(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    // Get the data
    $params = (array)$request->getParsedBody();
    $name = isset2($params['name']);
    $red = isset2($params['red']);
    $green = isset2($params['green']);
    $blue = isset2($params['blue']);
    // Validations
    if (is_null($name) || is_null($red) || is_null($green) || is_null($blue)) {
      return $response->withStatus(400);
    }
    if (!is_numeric($red) || !is_numeric($green) || !is_numeric($blue)) {
      return $response->withStatus(400);
    }
    if (!between($red, 0, 255) || !between($green, 0, 255) || !between($blue, 0, 255)) {
      return $response->withStatus(400);
    }
    $name = trim($name);
    if (strlen($name) == 0) {
      return $response->withStatus(400);
    }
    $color = new Color(null, $name, $red, $green, $blue);
    $color = ColorDAO::insertColor($color);
    if (is_null($color)) {
      // Color was not created
      return $response->withStatus(400);
    }
    else {
      // Return the color created (and inform of the new id)
      return $response->withJson($color);
    }
  }

  public function patchColorById(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $id = $args['id'];
    if (!is_numeric($id)) {
      // id must be a numeric value
      return $response->withStatus(400);
    }
    $color = ColorDAO::getColorById($id);
    if (is_null($color)) {
      // Color was not found in DB
      return $response->withStatus(404);
    }
    // Get the data
    $params = (array)$request->getParsedBody();
    $name = isset2($params['name']);
    $red = isset2($params['red']);
    $green = isset2($params['green']);
    $blue = isset2($params['blue']);
    // Validations
    if (!is_null($name)) {
      $name = trim($name);
      if (strlen($name) > 0) {
        $color->setName($name);
      }
    }
    if (!is_null($red) && is_numeric($red) && between($red, 0, 255)) {
      $color->setRed($red);
    }
    if (!is_null($green) && is_numeric($green) && between($green, 0, 255)) {
      $color->setGreen($green);
    }
    if (!is_null($blue) && is_numeric($blue) && between($blue, 0, 255)) {
      $color->setBlue($blue);
    }
    $color = ColorDAO::updateColor($color);
    if (is_null($color)) {
      // Color was not updated
      return $response->withStatus(400);
    }
    else {
      // Return the color updated
      return $response->withJson($color);
    }
  }

  public function deleteColorById(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
    $id = $args['id'];
    if (!is_numeric($id)) {
      // id must be a numeric value
      return $response->withStatus(400);
    }
    $result = ColorDAO::deleteColorById($id);
    if ($result) {
      // Color was deleted from DB
      return $response->withStatus(200);
    }
    else {
      // Color was not found in DB
      return $response->withStatus(404);
    }
  }
}