<?php
namespace App;

class Logger {
    private static ?\Monolog\Logger $logger = null;

    public static function getInstance() {
        if (static::$logger == null) {
            static::$logger = new \Monolog\Logger('API REST');
            static::$logger->pushHandler(new \Monolog\Handler\StreamHandler(
                LOG_FOLDER . '/rest.log', \Monolog\Logger::INFO
            ));
        }
        return static::$logger;
    }
}