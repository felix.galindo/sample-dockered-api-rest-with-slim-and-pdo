<?php
namespace App;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Middleware\ErrorMiddleware;

class App {
  private $slim;

  public function __construct() {
    $this->slim = AppFactory::create();
    $this->initRoutes();
    $callableResolver = $this->slim->getCallableResolver();
    $responseFactory = $this->slim->getResponseFactory();
    $logger = \App\Logger::getInstance();
    $errorMiddleware = $this->slim->addErrorMiddleware(false, true, true, $logger);
    $this->slim->add(new \App\Middleware\JsonBodyParserMiddleware());
  }

  public function processRequest() {
    $this->slim->run();
  }

  public function initRoutes() {
    \App\Controller\HelloController::initRoutes($this->slim);
    \App\Controller\ColorController::initRoutes($this->slim);
  }
}