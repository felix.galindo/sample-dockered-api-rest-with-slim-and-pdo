#!/bin/bash
SCRIPTPATH=$(realpath "$0")
BINDIR=$(dirname "$SCRIPTPATH")
APPDIR=$(dirname "$BINDIR")
source $APPDIR/docker.env 
docker exec --interactive --tty $(docker ps | grep "api_db" | cut -d " " -f 1) \
  mysql -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE
