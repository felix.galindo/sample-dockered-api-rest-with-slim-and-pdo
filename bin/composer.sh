#!/bin/bash
SCRIPTPATH=$(realpath "$0")
BINDIR=$(dirname "$SCRIPTPATH")
APPDIR=$(dirname "$BINDIR")
docker run --rm --interactive --tty --volume "${APPDIR}/app":/app composer $1