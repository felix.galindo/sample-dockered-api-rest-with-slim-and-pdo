
-- Creates a sample table to store the color entities
CREATE TABLE colors (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255),
  red INT,
  green INT,
  blue INT
);

-- Insert some initial data
INSERT INTO colors VALUES (1, 'White', 255, 255, 255);
INSERT INTO colors VALUES (2, 'Red', 255, 0, 0);
INSERT INTO colors VALUES (3, 'Green', 0, 255, 0);
INSERT INTO colors VALUES (4, 'Blue', 0, 0, 255);
INSERT INTO colors VALUES (5, 'Cyan', 0, 255, 255);
INSERT INTO colors VALUES (6, 'Magenta', 255, 0, 255);
INSERT INTO colors VALUES (7, 'Yellow', 255, 255, 0);
INSERT INTO colors VALUES (8, 'Black', 0, 0, 0);
